from __future__ import unicode_literals

from django.db import models
from base.models import BaseUser

# Create your models here.


class Staff(models.Model):
    staff_user = models.OneToOneField(BaseUser, on_delete=models.CASCADE)
    staff_id = models.CharField(max_length=50)
    staff_admin_privileges = models.BooleanField(default=False)
    staff_salary = models.CharField(max_length=10)
    staff_joining_date = models.DateField()
    staff_designation = models.CharField(max_length=20)

    def __str__(self):
        return self.staff_user.base_user.first_name


class Notification(models.Model):
    notification_title = models.CharField(max_length=50)
    notification_body = models.CharField(max_length=1500)
    notification_timestamp = models.DateTimeField()
    notification_sender = models.ForeignKey(Staff)

    def __str__(self):
        return self.notification_title