# -*- coding: utf-8 -*-
# Generated by Django 1.9.7 on 2016-10-12 08:30
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('base', '0002_auto_20161012_1400'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('notification_title', models.CharField(max_length=50)),
                ('notification_body', models.CharField(max_length=1500)),
                ('notification_timestamp', models.DateTimeField()),
            ],
        ),
        migrations.CreateModel(
            name='Staff',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('staff_id', models.CharField(max_length=50)),
                ('staff_admin_privileges', models.BooleanField(default=False)),
                ('staff_salary', models.CharField(max_length=10)),
                ('staff_joining_date', models.DateField()),
                ('staff_designation', models.CharField(max_length=20)),
                ('staff_user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='base.BaseUser')),
            ],
        ),
        migrations.AddField(
            model_name='notification',
            name='notification_sender',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='staff.Staff'),
        ),
    ]
