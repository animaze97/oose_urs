from django.contrib import admin
from .models import Staff, Notification
# Register your models here.


admin.site.register(Staff)
admin.site.register(Notification)