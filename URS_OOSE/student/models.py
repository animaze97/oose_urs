from __future__ import unicode_literals
from django.db import models
from base.models import BaseUser

# Create your models here.


class Student(models.Model):

    student_user = models.ForeignKey(BaseUser, on_delete=models.CASCADE)
    student_program = models.CharField(max_length=30)
    student_branch_preference = models.CharField(max_length=1000)
    student_address = models.CharField(max_length=80)
    student_telephone = models.CharField(max_length=11)
    student_mobile = models.CharField(max_length=11)
    student_nationality = models.CharField(max_length=30)
    student_category = models.CharField(max_length=10)
    student_marital_status = models.CharField(max_length=10)
    student_fathers_name = models.CharField(max_length=40)
    student_mothers_name = models.CharField(max_length=40)
    student_link_class_12 = models.CharField(max_length=200)
    student_link_class_10 = models.CharField(max_length=200)
    student_link_category = models.CharField(max_length=200)
    student_link_passing_certificate = models.CharField(max_length=200)
    student_link_photograph = models.CharField(max_length=200)
    student_link_status = models.CharField(max_length=20)
    student_link_seat_allotted = models.CharField(max_length=30)

    def __str__(self):
        return self.base_user.first_name


class Examination(models.Model):
    examination_student = models.ForeignKey(Student, on_delete=models.CASCADE)
    examination_board_university = models.CharField(max_length=100)
    examination_school_college = models.CharField(max_length=100)
    examination_year_of_passing = models.CharField(max_length=4)
    examination_aggregate_percentage = models.CharField(max_length=2)
    examination_subjects = models.CharField(max_length=100)

    def __str__(self):
        return self.examination_board_university

