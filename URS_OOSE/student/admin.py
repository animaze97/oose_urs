from django.contrib import admin
from .models import Student, Examination
# Register your models here.

admin.site.register(Student)
admin.site.register(Examination)