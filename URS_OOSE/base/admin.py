from django.contrib import admin
from .models import BaseUser, Branch

# Register your models here.

admin.site.register(BaseUser)
admin.site.register(Branch)
