from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class BaseUser(models.Model):
    """
    The primary attributes of the default user are:

    username
    password
    email
    first_name
    last_name

    """
    base_user = models.OneToOneField(User, on_delete=models.CASCADE)
    base_dob = models.DateField()
    base_age = models.CharField(max_length=3)
    base_gender = models.CharField(max_length=20)
    base_role = models.CharField(max_length=20,null=True,blank=True)

    def __str__(self):
        return self.base_user.first_name


class Branch(models.Model):
    branch_code = models.CharField(max_length=3)
    branch_name = models.CharField(max_length=40)
    branch_total_seats = models.CharField(max_length=4)
    branch_available_seats = models.CharField(max_length=4)

    def __str__(self):
        return self.branch_name
