from django.shortcuts import render, HttpResponse, HttpResponseRedirect, render_to_response
import json
import sys
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from django.template import RequestContext

from rest_framework_jwt.settings import api_settings
jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

from models import BaseUser


def home(request):
    return render(request, 'base/base.html', {})


@csrf_exempt
def register(request):
    if 'first_name' in request.POST:
        first_name = request.POST['first_name']
        if not first_name:
            return render(
                request, "base/register.html",
                {"error": True, "message": "There are unfilled fields , please fill "
                                           "them before submitting", "status": 400})
        else:
            base_user = None
            posted_first_name = None
            posted_last_name = None
            posted_email = None
            posted_password = None
            posted_dob = None
            posted_gender = None
            try:
                if request.method == 'POST':
                    # Email and username are the same for our users
                    # Get all the information required to make a BaseUser object

                    posted_first_name = request.POST['first_name']
                    posted_last_name = request.POST['last_name']
                    posted_email = request.POST['email']
                    posted_password = request.POST['password']
                    posted_dob = request.POST['dob']
                    posted_gender = request.POST['gender']

                    # Check if a base_user already exists with the email the user is trying to register with
                    # Notify user that a base_user with the email already exists
                    base_user = get_base_user_user_by_email(posted_email)

                if base_user is not None:
                    return render(
                        request, "base/register.html",
                        {"error": True, "message": "A user is already associated with this Email Id", "status": 410})

                # Email Id is not taken so a base_user can be created
                base_user = create_base_user(posted_first_name, posted_last_name, posted_email, posted_password,
                                             posted_dob, posted_gender)
                if base_user is not None:
                    return render(request, "base/base.html", {"error": True, 'message': "Registration Successful", "status": 200})

                return render(
                    request, "base/register.html",
                    {"error": True, 'message': "Registration Error", "status": 410})

            except BaseException, e:
                print str(e) + str(sys.exc_traceback.tb_lineno)
                return render(
                    request, "base/register.html",
                    {"error": True, 'message': "Registration Error1", "status": 410})

    return render(request, 'base/register.html', {})


def login(request):
    return render(request, 'base/login.html', {})


def get_base_user_user_by_email(email):
    try:
        base_user = BaseUser.objects.get(email=email)
        return base_user
    except Exception:
        return None


def create_base_user(first_name_arg, last_name_arg, email_arg, password_arg, dob_arg, gender_arg):
    user = None
    try:
        # Create a new user object unique with email (as username) and password
        user = User.objects.create_user(username=email_arg, password=password_arg, first_name=first_name_arg,
                                        last_name=last_name_arg, email=email_arg)

        # If we were able to create a user object then create a Base user and return the object
        # If we were not able to create a user object then return None
        if user is not None:
            base_user = BaseUser(base_user=user, base_dob=dob_arg, base_age="", base_gender=gender_arg, base_role="")
            base_user.save()

            if base_user is not None:
                return base_user

            # If there is an error that messed up user creation then delete the user that was just created
            user.delete()

        return None
    except BaseException, e:
        if user:
            user.delete()
        print str(e) + str(sys.exc_traceback.tb_lineno)
        return None
